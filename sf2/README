AVL-Drumkits
============

Created by: Glen MacArthur 2015
Formats: SFZ, SF2, h2drumkit
Website: http://www.bandshed.net/avldrumkits/
Contact: info@bandshed.net

License
=======

CC-BY-SA with the following exception:
The by-sa condition is imperative if you modify the samples
themselves or create new sample libraries with this licensed product.
However, produced music and other non-sample-library works can be
licensed freely.

Attribution means keeping all authors information and this readme-file or
its content in the release.
If you want to create a fork, a modified version, of this sample library
you have to release and distribute it under a different name.

http://creativecommons.org/licenses/by-sa/3.0/


NOTE
====

These files were modified from the original .sf2 files from
http://www.bandshed.net/avldrumkits/

 - The panning was changed to match the layout as displayed on the images
   (hand-clap, maracas, tambourine)
 - silence was cut at the start of some samples
 - exclusive class groups were added for hi-hat and all cymbals.
 - Generic renaming and renumbering of all samples in all Kits within thei
   16 character SF2 limit.
 - Reordered all affected samples that were in incorrect velocity order.
 - Reset loop start points on a handful of Blonde Bop samples needing adjustment.
 - Adjusted sample root key number for Maracas on Red Zeppelin and
   Black Pearl from ‘64’ to ‘61’.
 - Updated and improved Sidestick, Snare and Snare Edge samples in Red Zeppelin
   with an alternate Ludwig 14" snare.
